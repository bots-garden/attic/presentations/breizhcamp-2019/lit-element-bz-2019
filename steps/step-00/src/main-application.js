import { LitElement, html } from 'lit-element'

export class MainApplication extends LitElement {
  
  render() {
    return html`
      <section class="container">
        <div>
          <h1 class="title">👋 Hello World 🌍</h1>
        </div>
      <section>
    `
  }
}

customElements.define('main-application', MainApplication)
