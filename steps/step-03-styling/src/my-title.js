import { LitElement, html } from 'lit-element';

export class MyTitle extends LitElement {

  render(){

    return html`
      <link rel="stylesheet" href="../css/amazing.css" />
      <h1 class="title">
        Skeleton 🤖 [LitElement]
      </h1> 
    `
  }
}
customElements.define('my-title', MyTitle)