import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';

export class MyAmazingButton extends LitElement {

  static get properties() {
    return {
      counter: { type: Number }
    }
  }
  
  constructor() {
    super()
    this.counter = 0
  }
  
  render(){
    return html`
      ${styles}
      <a 
        @click="${this.onClick}"
        class="button is-medium is-dark is-rounded">
        👋 Click me! ${this.counter}
      </a>
    `
  }

  onClick() {
    this.counter+=1
  }
}
customElements.define('my-amazing-button', MyAmazingButton)