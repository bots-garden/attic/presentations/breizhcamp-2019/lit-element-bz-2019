import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';

export class MyField extends LitElement {

  render(){
    return html`
      ${styles}

      <div class="field">
        <div class="control">
          <input 
            id="my_field"
            class="input is-medium" 
            type="text" 
            placeholder="👋 Hello World 🌍"
            @input=${
              () =>
                this.shadowRoot.querySelector('p').innerHTML 
                  = this.shadowRoot.querySelector('input').value
            }
          >
        </div>
        <p class="help is-size-4">This is a help text</p>
      </div>
    `
  }

}
customElements.define('my-field', MyField)