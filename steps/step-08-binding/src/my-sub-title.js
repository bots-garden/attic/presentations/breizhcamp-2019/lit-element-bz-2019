import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';


export class MySubTitle extends LitElement {

  static get properties() {
    return {
      myText: { attribute: 'my-text' }
    }
  }


  render(){
    return html`
      ${styles}
      <h2 class="subtitle is-2">
        ${this.myText}
      </h2>    
    `
  }

}
customElements.define('my-sub-title', MySubTitle)