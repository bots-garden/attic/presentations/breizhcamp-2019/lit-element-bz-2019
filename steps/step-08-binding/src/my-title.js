import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';


export class MyTitle extends LitElement {

  render(){

    return html`

      ${styles}

      <h1 class="title is-1">
        Skeleton 🤖 [LitElement]
      </h1> 
    `
  }
}
customElements.define('my-title', MyTitle)