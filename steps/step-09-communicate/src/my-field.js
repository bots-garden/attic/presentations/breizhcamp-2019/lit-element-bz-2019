import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';


export class MyField extends LitElement {

  static get properties() {
    return {
      message: { type: String }
    }
  }

  constructor() {
    super()
    this.message = "This is a help text"
  }
  
  render(){

    return html`

      ${styles}

      <div class="field">
        <div class="control">
          
          <input 
            id="my_field"
            class="input is-medium" 
            type="text" 
            placeholder="👋 Hello World 🌍"
            @input=${
              () => this.message = this.shadowRoot.querySelector('input').value
            }
          >
        </div>
        <p class="help is-size-4">${this.message}</p>
        <slot></slot>
      </div>
    `
  }


}
customElements.define('my-field', MyField)