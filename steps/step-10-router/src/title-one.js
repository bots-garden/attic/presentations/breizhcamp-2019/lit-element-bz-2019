import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';

export class TitleOne extends LitElement {
  render(){
    return html`
      ${styles}
      <h1 class="title is-1">One</h1>
    `
  }
}
customElements.define('title-one', TitleOne)
