import { LitElement, html } from 'lit-element'
import {styles} from '../styles/styles.js'
// 👋 this is the store
import { store } from '../store.js'

export class DecrementButton extends LitElement {
  
  render(){
    return html`
      ${styles}
      <a 
        @click="${this.onClick}"
        class="button is-medium is-dark is-rounded">
        😠 Decrement
      </a>
    `
  }

  onClick() {
    store.dispatch({ type: 'DECREMENT' })
  }
}
customElements.define('decrement-button', DecrementButton)