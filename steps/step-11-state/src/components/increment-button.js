import { LitElement, html } from 'lit-element'
import {styles} from '../styles/styles.js'
// 👋 this is the store
import { store } from '../store.js'

export class IncrementButton extends LitElement {
  
  render(){
    return html`
      ${styles}
      <a 
        @click="${this.onClick}"
        class="button is-medium is-dark is-rounded">
        😃 Increment
      </a>
    `
  }

  onClick() {
    store.dispatch({ type: 'INCREMENT' })
  }
}
customElements.define('increment-button', IncrementButton)