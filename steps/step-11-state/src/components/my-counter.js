import { LitElement, html } from 'lit-element'
import {styles} from '../styles/styles.js'
// 👋 this is the store
import { store } from '../store.js'

export class MyCounter extends LitElement {

  static get properties(){
    return {
      counter: Number
    }
  }

  render(){

    return html`
      ${styles}
      <h1 class="title is-1">
        ${this.counter}
      </h1> 
    `
  }
  // initialize at first change
  firstUpdated(changedProperties) {
    this.counter = store.getState()
    store.subscribe(() => {
      this.counter = store.getState()
    })
  }


}
customElements.define('my-counter', MyCounter)